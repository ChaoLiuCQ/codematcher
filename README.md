Introduction
============
Within this archive you find the replication package of CodeMatcher for the article "Simplifying Deep-Learning-Based Model for Code Search" by Chao Liu, Xin Xia, David Lo, Ahmed E. Hassan, and Shanping Li for currently under review at ACM Transactions on Software Engineering and Methodology. The aim of this replication package is to allow other researchers to replicate our results with minimal effort, as well as to provide additional results that could not be included in the article directly. 


Data Contents
========

data/janalyzer: the code parsed by our tool [Janalyzer](https://github.com/liuchaoss/janalyzer) from the [raw project data](https://bitbucket.org/ChaoLiuCQ/codebase).

data/parse: the parsed data from data/janalyzer by the parse.py script.

data/es: the formated data for elasticsearch from data/parse by the es.py script.

data/search: the returned results by the elasticsearch for each fuzzy query.


How to Use
==========
Get all data: https://pan.baidu.com/s/1awOjQbZeu9P_7YZCinVJ-g with permanent key(x74v)

Install and start ElasticSearch: https://www.elastic.co

Created index for ElasticSearch: create_index() in elasticsearch.py

Fill data into ElasticSearch: fill_data() in elasticsearch.py

Perform fuzzy search on ElasticSearch: fuzzy_search() in elasticsearch.py

Rerank the code list: rank.py


Codebase and Baseline models
============================
Codebase: https://bitbucket.org/ChaoLiuCQ/codebase

Janalyzer for code prepropcessing: https://github.com/liuchaoss/janalyzer

Baseline model DeepCS: https://bitbucket.org/ChaoLiuCQ/deepcs

Baseline model CodeHow: https://bitbucket.org/ChaoLiuCQ/codehow

Baseline model UNIF: https://bitbucket.org/ChaoLiuCQ/unif








